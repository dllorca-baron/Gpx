package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.basex.api.client.ClientSession;

import server.RunServerBaseX;
import server.RunServerDB4O;
import xml.ParserXml;

/**
 * Controlador de la base de datos sql.
 * 
 * @author David Llorca - iam47873216G
 *
 */
public class DaoSql {

    /** Conexión a la base de datos postgres */
    static Connection connection;

    public DaoSql() {
	try {
	    Class.forName("org.postgresql.Driver");
	    this.connection = DriverManager.getConnection(
		    "jdbc:postgresql://localhost:5432/gpx", "test", "123456");

	    // System.out.println("conexion closed?: " + connection.isClosed());

	} catch (ClassNotFoundException | SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /**
     * Inserta un nuevo usuario en la tabla usuarios.
     * 
     * @param connection
     * @param usuario
     * @param password
     */
    public void crearUsuario(String usuario, String password) {
	try {
	    String query = ("INSERT into usuarios (nombre, pass) VALUES(?, ?)");
	    PreparedStatement pSt = connection.prepareStatement(query);

	    pSt.setString(1, usuario);
	    pSt.setString(2, password);

	    pSt.execute();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    /**
     * Verifica la existencia de un usuario en la bades de datos.
     * 
     * @param connection
     * @param usuario
     * @return true si el usuario existe y false en caso contrario.
     */
    public boolean existeUsuario(String usuario) {
	try {
	    String query = "select count(*) from usuarios where nombre=?";
	    PreparedStatement pst = connection.prepareStatement(query,
		    ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.NO_GENERATED_KEYS);
	    pst.setString(1, usuario);
	    ResultSet rs = pst.executeQuery();

	    if (rs != null) {
		// Movemos el cursor hasta la última fila aunque se supone que
		// sólo se ha devuelto una.
		rs.last();
		// n será la cantidad de usuarios devuelta en el COUNT(*)
		int n = rs.getInt(1);
		if (n != 0)
		    return true;
	    }

	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return false;
    }

    /**
     * Autentifica que el par, usuario y contraseña, sean correctos.
     * 
     * @param connection
     * @param usuario
     * @param password
     * @return true si el usuario y la contraseña son correctos y false en caso
     *         contrario.
     */
    public boolean validarUsuario(String usuario, String password) {
	try {
	    String query = "select * from usuarios where nombre=?";
	    PreparedStatement pst = connection.prepareStatement(query,
		    ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.NO_GENERATED_KEYS);
	    pst.setString(1, usuario);
	    ResultSet rs = pst.executeQuery();

	    if (rs != null) {
		// Movemos el cursor hasta la última fila aunque se supone que
		// sólo se ha devuelto una.
		rs.last();
		// Obtenemos el contenido de la columna 'user' y 'pass'.
		String user = rs.getString(2);
		String pass = rs.getString(3);
//		System.out.println(user + "  " + pass);
		// Si el usuario y el password coincide con los datos en la
		// tabla usuarios devuelve true.
		if (user.equals(usuario) && pass.equals(password))
		    return true;
	    }

	} catch (SQLException e) {
	    // TODO Auto-generated catch block
    	    e.printStackTrace();
    	    return false;
	} catch (Exception e) {
	    // TODO: handle exception
	}
	return false;
    }

    /**
     * Lista la tabla usuarios. Imprime por pantalla.
     * 
     * @param connection
     */
    public void listarUsuarios() {
	try {
	    Statement st = connection.createStatement();
	    ResultSet rs = st.executeQuery("select * from usuarios");

	    if (rs != null) {
		while (rs.next()) {
		    // Podemos acceder con la posición o con el nombre de la
		    // columna.
		    System.out.print(rs.getString(1) + "\t");
		    System.out.print(rs.getString(2) + "\t"); // empieza por 1
		    System.out.print(rs.getString(3) + "\n");
		}
	    }
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /**
     * Devuelve el identificador de un usuario.
     * 
     * @param connection
     * @param usuario
     * @return el id de usuario o null en caso de no existir.
     */
    public String getIdUsuario(String usuario) {
	String idUsuario = null;
	try {
	    String query = "select id from usuarios where nombre=?";
	    PreparedStatement pst = connection.prepareStatement(query,
		    ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.NO_GENERATED_KEYS);
	    pst.setString(1, usuario);
	    ResultSet rs = pst.executeQuery();

	    if (rs != null) {
		rs.last();
		// Obtenemos el contenido de la columna 'id' que es la única que
		// ha devuelto.
		idUsuario = rs.getString(1);
		return idUsuario;
	    }

	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return idUsuario;
    }

    /**
     * Inserta la ruta en la base de datos postgres.
     * 
     * @param connection
     * @param usuario
     * @param nombrex
     *            nombre del fichero guardado en BaseX
     * @param xml
     *            parser del fichero xml que contendra la ruta.
     */
    public void anadirRuta(String usuario, String nombrex, ParserXml xml) {
	try {
	    String query = ("INSERT into rutas (nombre, descripcion, desnivel, nombrex, user_id) VALUES(?, ?, ?, ?, ?)");
	    PreparedStatement pSt = connection.prepareStatement(query);

	    // Añade los values a la query
	    pSt.setString(1, xml.getNombre());
	    pSt.setString(2, xml.getDescripcion());
	    pSt.setDouble(3, Double.parseDouble(xml.getDesnivelPositivo()));
	    pSt.setString(4, nombrex);
	    pSt.setInt(5, Integer.parseInt(getIdUsuario(usuario)));

	    pSt.execute();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /**
     * Muestra el id de la última ruta añadida.
     * 
     * @param connection
     * @return el id de la ruta o 0 en caso de no encontrar ninguna ruta.
     */
    public int getIdRuta() {
	int idRuta = 1;
	try {
	    String query = ("SELECT MAX(id) from rutas");
	    PreparedStatement pSt = connection.prepareStatement(query,
		    ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.NO_GENERATED_KEYS);
	    ResultSet rs = pSt.executeQuery();

	    if (rs != null) {
		rs.last();
		// Obtenemos el contenido de la columna 'id' que es la única que
		// ha devuelto.
		idRuta = rs.getInt(1);
		//System.out.println("idRuta:" + idRuta);
//		if (idRuta == 0)
//		    return 1;
		return idRuta;
	    }

	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
	System.err.println("idRuta: " + idRuta);
	return idRuta;

    }

    /**
     * Devuelve el id del último documento añadido a al bd postgres.
     * 
     * @param connection
     * @return el id del documento(fichero media)
     */
    public int getIdDocumento() {
	int idDocumento = 1;
	try {
	    String query = ("SELECT MAX(id) from documentos");
	    PreparedStatement pSt = connection.prepareStatement(query,
		    ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.NO_GENERATED_KEYS);
	    ResultSet rs = pSt.executeQuery();

	    if (rs != null) {
		rs.last();
		// Obtenemos el contenido de la columna 'id' que es la única que
		// ha devuelto.
		idDocumento = rs.getInt(1);
//		if (idDocumento == 0)
//		    return 1;
		//System.out.println("idDocumento" + idDocumento);
		return idDocumento;
	    }

	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	System.err.println("idDocumento" + idDocumento);

	return idDocumento;

    }

    /**
     * Inserta el nombre del fichero en la bd postgres.
     * 
     * No inserta el fichero, sino una referencia a este.
     * 
     * @param connection
     * @param nombreFichero
     * @param idRuta
     */
    public void insertarDocumento(String nombreFichero, int idRuta) {
	try {
	    String query = ("INSERT into documentos (nombre, id_Ruta) VALUES(?, ?)");
	    PreparedStatement pSt = connection.prepareStatement(query);

	    // Añade los values a la query
	    pSt.setString(1, nombreFichero);
	    pSt.setInt(2, idRuta);

	    pSt.execute();
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public String[] getRutasUsuario(String usuario) {
	ArrayList<String> list = new ArrayList<String>();
	try {
	    // Devuelve la id del usuario
	    String idUser = getIdUsuario(usuario);

	    Statement st = connection.createStatement();
	    ResultSet rs = st.executeQuery("select nombrex from rutas where user_id='" + idUser
		    + "'");

	    if (rs != null) {
		while (rs.next()) {
		    list.add(rs.getString(1));
		}
	    }
	    String[] rutas = new String[list.size()];
	    return list.toArray(rutas);
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return null;

    }

    public void main(String[] args) {

	Connection connection = null;
	try {
	    Class.forName("org.postgresql.Driver");
	    connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/gpx",
		    "test", "123456");

	    // System.out.println("conexion closed?: " + connection.isClosed());

	} catch (ClassNotFoundException | SQLException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	System.out.println(getIdRuta());
	System.out.println(getIdDocumento());
    }
}
