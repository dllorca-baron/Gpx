package controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.BaseXException;
import org.basex.core.cmd.Check;
import org.basex.core.cmd.CreateDB;
import org.w3c.dom.Document;

import util.Util;

/**
 * Controlador de la base de datos BaseX.
 * 
 * @author David Llorca - iam47873216
 *
 */
public class DaoBaseX {

    private ClientSession session;

    public DaoBaseX() {
	this.session = null;
	try {
	    session = new ClientSession("localhost", 1984, "admin", "admin");
	 // Crea la base de datos si no existe y la abre en caso de existir.
	    session.execute(new Check("databaseGpx"));
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
    }

    /**
     * Inserta resumen de la ruta en xml eb BaseX
     * 
     * @param filePath
     *            ruta donde se encuentra el fichero del resumen.
     * @param nombreRuta
     *            nombre con el que se guardará el fichero resumen en xml.
     */
    public void insertarXml(String filePath, String nombreRuta) {

	try {
	    // Añadir un xml
	    Path path = Paths.get(filePath);
	    InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));

	    // Asignarle nombre a la ruta
	    // session.add("/xml/" + nombreRuta + ".xml", is);
	    session.store(nombreRuta + ".xml", is);
	    System.out.println("Ruta insertada: " + nombreRuta + " - " + session.info());

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /**
     * Recupera el xml con el resumen de la ruta.
     * 
     * @param nombrex
     *            nombre del fichero en BaseX.
     */
    public void retrieveResumenXml(String nombrex) {
	try {

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    session.setOutputStream(baos);
	    session.execute("retrieve " + nombrex + ".xml");
	    //System.out.println(session.info());

	    FileOutputStream fileout = new FileOutputStream(new File("/tmp/" + nombrex
		    + "_recuperada.xml"));
	    baos.writeTo(fileout);

	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Recupera el fichero rutas.xml (todas las rutas de la bd) y crea una copia
     * en /tmp/ para manipularlo.
     * 
     * @return
     */
    public boolean retrieveRutas() {
	try {

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    session.setOutputStream(baos);
	    session.execute("retrieve rutas.xml");
	    //System.out.println(session.info());

	    File rutas = new File("/tmp/rutas_recuperado.xml");
	    FileOutputStream fileout = new FileOutputStream(rutas);
	    baos.writeTo(fileout);

	    return true;
	} catch (IOException e) {
	    return false;

	}
    }

    /**
     * Elimina el fichero temporal rutas._recuperadoxml
     */
    public void deleteTmpRutas() {
	File rutas = new File("/tmp/rutas_recuperado.xml");
	rutas.delete();
    }

    /**
     * Lista los fichero de la base de datos "databaseGpx".
     */
    public void listFilesBaseX() {
	try {
	    System.out.println(session.execute("list databaseGpx"));
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /**
     * Inserta un fichero en BaseX
     * 
     * @param nombreFichero
     */
    public void insertarFicheroMedia(String nombreFichero) {

	try {
	    // define input stream
	    final byte[] bytes = new byte[256];
	    for (int b = 0; b < bytes.length; b++)
		bytes[b] = (byte) b;
	    final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);

	    // add document
	    session.store(nombreFichero, bais);
	    System.out.println("Fichero media insertado: " + session.info());

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

}
