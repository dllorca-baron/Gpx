/**
 * 
 */
package controller;

import model.Accion;
import model.Ruta;
import model.Usuario;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.cs.Db4oClientServer;
import com.db4o.query.Constraint;
import com.db4o.query.Query;

/**
 * Controlador de la base de datos Db4o.
 * 
 * @author David Llorca - iam47873216G
 *
 */
public class DaoDb4o {

    public ObjectContainer db;

    public DaoDb4o() {
	this.db = connectionDB("db4oGpx");
    }

    /**
     * Devolvera una conexión a la base de datos.
     * 
     * @param database
     * @return
     */
    public ObjectContainer connectionDB(String database) {

	// Conexión con la base de datos, si no existe la crea
	ObjectContainer db = Db4oEmbedded.openFile(database);
	return db;
    }

    /**
     * Crea un objecto usuario en Db4o.
     * 
     * @param idUsuario
     * @param nombre
     */
    public void crearUsuario(int idUsuario, String nombre) {

	Usuario usuario = new Usuario(idUsuario, nombre);
	// Guardamos la instancia del Usuario.
	db.store(usuario);

	// Guarda la acción en el historial
	addLogAltaUsuario(true, idUsuario);

    }

    /**
     * Recupera un Usuario.
     * 
     * @param idUsuario
     * @param nombre
     * @return una instancia del Usuario solicitado.
     */
    private Usuario recuperarUsuario(int idUsuario) {
	Usuario usuario = new Usuario(idUsuario);

	ObjectSet<Usuario> result = db.queryByExample(usuario);

	if (!result.isEmpty()) {
	    return result.next();
	} else {
	    System.out.println("No se ha encontrado el usuario");
	    return null;
	}
    }

    /**
     * Lista todos los usuarios
     */
    public void listarUsuarios() {

	Usuario usuario = new Usuario();

	// Busca todos los objetos de la clase Usuario que haya, buscará las
	// instancias tipe 'usuario'
	ObjectSet<Usuario> result = db.queryByExample(usuario);

	// Muestra los usuarios por pantalla.
	System.out.println("Numero de usuarios: " + result.size());
	while (result.hasNext()) {
	    Usuario e = result.next();
	    System.out.println(e.toString());
	}

    }

    /**
     * Crea un objecto usuario en Db4o.
     * 
     * @param ruta
     */
    public void anadirRuta(Ruta ruta) {

	db.store(ruta);

	// Añade la ruta al usuario
	Usuario usuario = recuperarUsuario(ruta.getIdUsuario());

	usuario.addRuta(ruta);
	usuario.addKms(ruta.getDistancia());
	// Guarda la acción en el historial
	addLogSubirRuta(true, ruta.getIdUsuario());
	db.store(usuario);

    }

    /**
     * Lista todos los usuarios
     */
    public void listarRutas() {

	Ruta ruta = new Ruta();

	// Busca todos los objetos de la clase Usuario que haya, buscará las
	// instancias tipe 'usuario'
	ObjectSet<Ruta> result = db.queryByExample(ruta);

	System.out.println("Numero de rutas: " + result.size());
	while (result.hasNext()) {
	    Ruta r = result.next();
	    System.out.println(r.toString());
	}

    }
    
    /**
     * Recupera todos los objetos Usuario
     * 
     * @return
     */
    public ObjectSet<Usuario> recuperarUsuarios() {
	Usuario usuario = new Usuario();

	// Busca todos los objetos de la clase Usuario que haya, buscará las
	// instancias tipe 'usuario'
	ObjectSet<Usuario> result = db.queryByExample(usuario);

	return result;
    }
    
    /**
     * Recupera todos los objectos Ruta
     * 
     * @return
     */
    public ObjectSet<Ruta> recuperarRutas() {
	Ruta ruta = new Ruta();

	// Busca todos los objetos de la clase Usuario que haya, buscará las
	// instancias tipe 'usuario'
	ObjectSet<Ruta> result = db.queryByExample(ruta);

	return result;
    }

    /**
     * Lista todas las acciones.
     */
    public void listarHistorial() {
	Accion accion = new Accion();

	// Busca todos los objetos de la clase Accion que haya, buscará las
	// instancias tipe 'accion'
	ObjectSet<Accion> result = db.queryByExample(accion);

	System.out.println("Numero de acciones: " + result.size());
	while (result.hasNext()) {
	    Accion a = result.next();
	    System.out.println(a.toString());
	}
    }
    
    /**
     * Recupera todos los objetos Acción.
     * @return
     */
    public ObjectSet<Accion> recuperarAcciones(){
	Accion accion = new Accion();

	// Busca todos los objetos de la clase Accion que haya, buscará las
	// instancias tipe 'accion'
	ObjectSet<Accion> result = db.queryByExample(accion);
	
	return result;
    }

    /**
     * MÉTODOS PARA AÑADIR LAS ACCIONES A LA DB DB4O
     */
    public void addLogAltaUsuario(boolean resultado, int idUsuario) {
	Accion accion = new Accion("Alta usuario", resultado, idUsuario);
	db.store(accion);
    }

    public void addLogSubirRuta(boolean resultado, int idUsuario) {
	Accion accion = new Accion("Subir ruta", resultado, idUsuario);
	db.store(accion);
	anadeAccionUsuario(idUsuario);
    }

    public void addLogConsultarRutasUsuario(boolean resultado, int idUsuario) {
	Accion accion = new Accion("Consultar rutas usuario", resultado, idUsuario);
	db.store(accion);
	anadeAccionUsuario(idUsuario);
    }

    public void addLogVerRuta(boolean resultado, int idUsuario) {
	Accion accion = new Accion("Ver ruta", resultado, idUsuario);
	db.store(accion);
    }

    public void addLogConsultarTotalRutas(boolean resultado, int idUsuario) {
	Accion accion = new Accion("Consultar total rutas", resultado, idUsuario);
	db.store(accion);
    }
    
    public void anadeAccionUsuario(int idUsuario){
	Usuario usuario = recuperarUsuario(idUsuario);
	usuario.addAcciones();
	db.store(usuario);
    }
    
    /*
     * CONSULTAS DB4O
     */
    
    /**
     * Devuelve el usuario con mas rutas.
     */
    public void usuarioConMasRutas(){
	
	Query q = db.query();
	q.constrain(Usuario.class);
	
	q.descend("totalRutas").orderDescending();
		
	ObjectSet<Usuario> result = q.execute();
	
	System.out.println(result.next().toString());	
    }
    
    /**
     * Devuelve el usuario que mas kms ha subido.
     */
    public void usuarioConMasKm() {
	Query q = db.query();
	q.constrain(Usuario.class);
	
	q.descend("kms").orderDescending();
		
	ObjectSet<Usuario> result = q.execute();
	
	System.out.println(result.next().toString());	
	
    }
    /**
     * Usuario que mas acciones ha realizado.
     */
    public void usuarioConMasAcciones() {
	Query q = db.query();
	q.constrain(Usuario.class);
	
	q.descend("acciones").orderDescending();
		
	ObjectSet<Usuario> result = q.execute();
	
	System.out.println(result.next().toString());	
    }
    
    /**
     * Acciones del usuario invitado.
     */
    public void getAccionesVisitante() {
	ObjectSet<Accion> result = recuperarAcciones();
	
	int iAcciones = 0;
	while (result.hasNext()) {
	    Accion a = result.next();
	    if (a.getUsuario() == -1){
		iAcciones++;
	    }
	}
	System.out.println("Acciones visitante: " + iAcciones);
    }
    
    /**
     * Usuario con una media de desnivel mayor. 
     */
    public void usuarioConMasDesnivel() {
	Query q = db.query();
	q.constrain(Usuario.class);
	
	q.descend("totalDesnivel").orderDescending();
		
	ObjectSet<Usuario> result = q.execute();
	
	System.out.println(result.next().toString());	
	
    }
    
    /**
     * Usuario que mas ha consultado sus rutas
     */
    public void usuarioRutasMasConsultadas() {
	
    }

    public void rutaMasCorta() {
	Query q = db.query();
	q.constrain(Ruta.class);
	
	q.descend("distancia").orderAscending();
		
	ObjectSet<Ruta> result = q.execute();
	
	System.out.println(result.next().toString());
	
    }

    public void rutaMasLarga() {
	Query q = db.query();
	q.constrain(Ruta.class);
	
	q.descend("distancia").orderDescending();
		
	ObjectSet<Ruta> result = q.execute();
	
	System.out.println(result.next().toString());
	
    } 
}
