package server;
import com.db4o.Db4o;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

/**
 * Clase controladora conexión con la base de datos Db4o.
 * 
 * @author David Llorca - iam47873216
 *
 */
public class ConectionDB {

	public static ObjectContainer conectionDB(String dataBase) {
		ObjectContainer db = Db4oEmbedded.openFile(dataBase);
		return db;
	}
}
