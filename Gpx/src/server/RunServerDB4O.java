package server;
import com.db4o.ObjectServer;
import com.db4o.cs.Db4oClientServer;

public class RunServerDB4O implements Runnable {

	private boolean stop = false;

	public void run() {
		synchronized (this) {
			ObjectServer server = Db4oClientServer.openServer(
					Db4oClientServer.newServerConfiguration(), "Nserver.yap",
					8733);

			Thread.currentThread().setName(this.getClass().getName());
			
			// Usuario para la conexión
			server.grantAccess("user1", "password");
			try {

				while (!stop) {
					System.out.println("SERVER Db4o:[" + System.currentTimeMillis()
							+ "] Server's running... ");
					this.wait(5*60000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				server.close();
			}
		}
	}
}
