package server;

import java.io.IOException;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;

public class RunServerBaseX implements Runnable {

    /** Servidor */
    private static BaseXServer server;
    private boolean stop = false;

    // Contructor

//    // Arranca el servidor al crear el objeto.
//    public RunServerBaseX() {
//	run();
//    }

    public void run() {
	synchronized (this) {
	    try {
		// iniciamos els servidor
		server = new BaseXServer();
		
//		// cliente para conectarnos al servidor
//		 ClientSession session = new ClientSession("localhost", 1984, "admin",
//		 "admin");
//		 
//		 session.execute("create db databaseGpx");
//		 System.out.println(session.info());
//		 session.close();
		while (!stop) {
		    System.out.println("SERVER BaseX:[" + System.currentTimeMillis()
			    + "] Server's running... ");
		    this.wait(5*60000);
		}

	    } catch (IOException e) {
		e.printStackTrace();
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    } finally {
		try {
		    server.stop();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}
    }
}
