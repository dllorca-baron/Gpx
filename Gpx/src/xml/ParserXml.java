package xml;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import model.Ruta;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import util.Util;

public class ParserXml {

    /** Documento xml con toda la info de la ruta */
    private Document document;
    private SAXParser saxParser;
    private String filename;

    // CONSTRUCTOR
    public ParserXml(String filename) {
	this.filename = filename;
	this.document = Util.createNewDocumentToParse(filename);
	SAXParserFactory factory = SAXParserFactory.newInstance();
	try {
	    this.saxParser = factory.newSAXParser();
	} catch (ParserConfigurationException | SAXException e) {
	    e.printStackTrace();
	}
    }

    // MÈTODOS

    /**
     * Recoge el contenido del tag<name>.
     *
     * @return nombre de la ruta.
     */
    public String getNombre() {
	NodeList nodeList = this.document.getElementsByTagName("name");
	Node node = nodeList.item(0);
	return node.getTextContent();
    }

    /**
     * Recoge el contenido del tag<desc>
     * 
     * @return descripción de la ruta.
     */
    public String getDescripcion() {
	NodeList nodeList = this.document.getElementsByTagName("desc");
	Node node = nodeList.item(0);
	String descripcion = node.getTextContent().substring(0, 40);
	return descripcion + "...";
    }

    /**
     * Calcula el desnivel positivo acumulado de toda la ruta.
     * 
     * @return desnivel positivo de la ruta.
     */
    public String getDesnivelPositivo() {

	// Lista con todos los nodos trkpt
	NodeList listaTrkpt = document.getElementsByTagName("trkpt");

	double dPositivo = 0;
	double elevacionAnterior = 0;

	Node firstNodo = listaTrkpt.item(0);
	NodeList nodeListFirstItem = firstNodo.getChildNodes();

	// Calculamos la primera elevación de referencia.
	for (int i = 0; i < nodeListFirstItem.getLength(); i++) {
	    if (nodeListFirstItem.item(i).getNodeName().equals("ele")) {
		elevacionAnterior = Double.parseDouble(nodeListFirstItem.item(i)
			.getTextContent());
	    }
	}

	for (int i = 0; i < listaTrkpt.getLength(); i++) {
	    Node nodo = listaTrkpt.item(i);
	    NodeList nodeList = nodo.getChildNodes();

	    for (int j = 0; j < nodeList.getLength(); j++) {
		if (nodeList.item(j).getNodeName().equals("ele")) {
		    double elevacionActual = Double.parseDouble(nodeList.item(j)
			    .getTextContent());
		    double diferencia = 0;
		    // Si es la diferencia es positiva (ascenso respecto el
		    // punto anterior)
		    if (elevacionActual - elevacionAnterior >= 0) {
			diferencia = elevacionActual - elevacionAnterior;
			dPositivo += diferencia;
			// System.out.println("Asciende :" + diferencia);
			// Si la diferencia es negativa (descenso respecto el
			// punto anterior)
		    }
		    // Cambiamos el punto de referencia.
		    elevacionAnterior = elevacionActual;
		}
	    }
	}
	dPositivo = Math.round(dPositivo);
	return dPositivo + "";
    }

    /**
     * Calcula la distancia entre todos los puntos de la ruta.
     * 
     * @return distancia total de la ruta
     */
    public String getDistancia() {
	double distancia = 0;
	try {
	    MyHandler handler = new MyHandler();
	    saxParser.parse(this.filename, handler);
	    distancia = Math.round(handler.puntos.getDistancia());
	} catch (SAXException | IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return distancia + "";
    }

    /**
     * Crea un Document DOM con los nodos requeridos en el resumen xml.
     * 
     * @return document
     */
    public Document createDocument() {
	Document document = Util.createNewDocumentToParse();
	
	// Nodo raíz
	Element ruta = document.createElement("ruta");
	document.appendChild(ruta);
	
	// Nodos del elemento raíz
	Element nombreRuta = document.createElement("nombre");
	nombreRuta.appendChild(document.createTextNode(getNombre()));
	Element descripcion = document.createElement("descripcion");
	descripcion.appendChild(document.createTextNode(getDescripcion()));
	Element desnivel = document.createElement("desnivelPositivo");
	desnivel.appendChild(document.createTextNode(getDesnivelPositivo()));
	Element distancia = document.createElement("distancia");
	distancia.appendChild(document.createTextNode(getDistancia()));

	ruta.appendChild(nombreRuta);
	ruta.appendChild(descripcion);
	ruta.appendChild(desnivel);
	ruta.appendChild(distancia);
	
	return document;
    }
    
    /**
     * Escribe el Document DOM en un fichero xml.
     * 
     * @param document
     * @return la ruta donde se ha creado el fichero de resume de ruta provisional.
     */
    public String writeDocumentToXml(Document document){
	String filePath = Util.writeDocumentToFileXml(document, "ruta");
	// Util.printFileXml(document);
	
	System.out.println("FilePath: " + filePath);
	return filePath;
	
    }
    
    public Ruta crearObjectoRuta(int idRuta, int idUsuario){
	Ruta ruta = new Ruta();
	ruta.setIdRuta(idRuta);
	ruta.setIdUsuario(idUsuario);
	ruta.setNombre(getNombre());
	ruta.setDescripcion(getDescripcion());
	ruta.setDistancia(Double.parseDouble(getDistancia()));
	ruta.setDesnivel(Double.parseDouble(getDesnivelPositivo()));
	
	return ruta;	
    }
    
}
