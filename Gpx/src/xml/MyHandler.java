package xml;
import java.util.List;

import model.Punto;
import model.Puntos;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MyHandler extends DefaultHandler {

	boolean trkpt = false;

	static final double RADIO_TERRESTRE = 6531;
	double lat = 0;
	double lon = 0;

	Puntos puntos = new Puntos();
	long distancia = 0;

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);

		if (qName.equalsIgnoreCase("trkpt")) {
			trkpt = true;
			for (int i = 0; i < attributes.getLength(); i++) {
				if (attributes.getQName(i).equals("lat")) {
					// System.out.println("lat:" + attributes.getValue(i));
					lat = Double.parseDouble(attributes.getValue(i));
				} else if (attributes.getQName(i).equals("lon")) {
					lon = Double.parseDouble(attributes.getValue(i));
				}
			}
			Punto p = new Punto(lat, lon);
			puntos.addPoint(p);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// TODO Auto-generated method stub
		super.endElement(uri, localName, qName);
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		// TODO Auto-generated method stub
		super.characters(ch, start, length);
	}

	@Override
	public void endDocument() throws SAXException {

		List<Punto> list = puntos.getListaPuntos();
		double latIni = list.get(0).getLatitud();
		double lonIni = list.get(0).getLongitud();
		double total = 0;

		for (int i = 0; i < list.size(); i++) {
			double latFi = list.get(i).getLatitud();
			double lonFi = list.get(i).getLongitud();

			double dLat = Math.toRadians(latFi - latIni);
			double dLon = Math.toRadians(lonFi - lonIni);

			// Fórmula Haversine
			double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
					+ Math.sin(dLon / 2) * Math.sin(dLon / 2)
					* Math.cos(Math.toRadians(latIni))
					* Math.cos(Math.toRadians(latFi));
			double c = 2 * Math.asin(Math.sqrt(a));
			double d = RADIO_TERRESTRE * c;
			//System.out.println(i + ": " + d + " km");
			// Aculuma el total de la distancia de la ruta.
			total += d;

			// Indexamos la variables
			latIni = latFi;
			lonIni = lonFi;
		}
		puntos.setDistancia(total);
		//System.out.println(total + " km");

	}

}
