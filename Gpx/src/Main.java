import java.util.Scanner;

import html.GenerateHtml;
import model.Ruta;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import util.Util;
import xml.ParserXml;
import controller.DaoBaseX;
import controller.DaoDb4o;
import controller.DaoSql;

/**
 * 
 * @author David Llorca - iam478743216
 *
 */
public class Main {

    public static void main(String[] args) {
	/**
	 * CONTROLADORES
	 */
	DaoBaseX daoBaseX = new DaoBaseX();
	DaoSql daoSql = new DaoSql();
	DaoDb4o daoDb4o = new DaoDb4o();

	// /**
	// * PRUEBAS
	// */
	// // Recogemos todos los argumentos y los mostramos por pantalla
	// for (int i = 0; i < args.length; i++) {
	// System.out.println(args[i]);
	// }

	/**
	 * DISCRIMINACIÓN POR ARGUMENTOS.
	 * 
	 * Primero comprueba que los argumentos sean válidos con el método
	 * validaArgumentos(String[] args) y en caso de serlo discrimina segun
	 * el primer argumento que se haya introducido en la instrucción.
	 */
	if (Util.validarArgumentos(args)) {

	    // Control de errores, mensaje a mostrar al usuario.
	    String tag;
	    String errorMessage = null;
	    String errorMessageGeneral = "Instrucción incorrecta. Revisa la sintaxis";

	    switch (args[0]) {
	    /*
	     * DAR DE ALTA UN USUARIO
	     * 
	     * En caso de aparecer la opción '-add' como primer argumento
	     * comprobaremos que la instrucción sólo consta de 3 argumentos. Se
	     * comprueba que el usuario no existe y que la contraseña tenga una
	     * longitud mínima de 6 caracteres. Ejemplo: '-add david passwd'.
	     */
	    case "-add":

		if (args.length == 3 && !daoSql.existeUsuario(args[1]) && args[2].length() > 5) {
		    // INSERT en la bd, tabla usuarios
		    daoSql.crearUsuario(args[1], args[2]);

		    // Crea el usuario en Db4O
		    daoDb4o.crearUsuario(Integer.parseInt(daoSql.getIdUsuario(args[1])),
			    args[1]);

		    // Muestra mensaje informando sobre la acción
		    System.out.println("Usuario creado correctamente");
		} else {
		    tag = "Error '" + args[0] + "': ";
		    // Añade una acción erronea al historial
		    daoDb4o.addLogAltaUsuario(false, -1);
		    if (args.length != 3) {
			errorMessage = "Numero de argumentos incorrecto. GPX -add user passwd";
		    } else if (daoSql.existeUsuario(args[1])) {
			errorMessage = "Usuario existente, pruebe otro nombre";
		    } else if (args[2].length() < 6) {
			errorMessage = "La contraseña debe contener mínimo 6 caracteres";
		    } else {
			errorMessage = errorMessageGeneral;
		    }
		    // Mostramos el mesaje de error por pantalla
		    System.err.println(tag + errorMessage);
		}
		break;
	    /*
	     * INDENTIFICAR USUARIO
	     * 
	     * En caso de aparecer la opción '-user' como primer argumento
	     * significará que se trata de una instrucción donde se requiere
	     * identificación del usuario y esta habrá de cumplir cierta
	     * sintaxis. La dos únicas instrucciones que requieren
	     * identificación son 'Subir ruta' y 'Consultar mis rutas'.
	     * Ejemplo:'-user david -pass passwd.
	     */
	    case "-user":

		// Comrpueba que el usuario exista y si el pass es correcto.
		boolean existeUsuario = daoSql.existeUsuario(args[1]);
		boolean passwordCorrecto = daoSql.validarUsuario(args[1], args[3]);

		if (args[2].equals("-pass") && args.length > 5 && existeUsuario
			&& passwordCorrecto) {
		    System.out.println("Autentificación correcta"); // Info por
								    // pantalla
		    int idUsuario = Integer.parseInt(daoSql.getIdUsuario(args[1]));
		    // Discrimina si el usuario quiere subir una ruta nueva
		    // o consultar sus rutas

		    /**
		     * AÑADIR RUTA NUEVA
		     */
		    if (args[4].equals("-ruta")) {

			// Crear resumen xml
			ParserXml parser = new ParserXml(args[5]);

			int idRuta = daoSql.getIdRuta();
			idRuta += 1;
			String nombreRuta = "ruta" + idRuta;

			// inserta nueva ruta en postgres
			daoSql.anadirRuta(args[1], nombreRuta, parser);

			Ruta ruta = parser.crearObjectoRuta(idRuta,
				Integer.parseInt(daoSql.getIdUsuario(args[1])));
			daoDb4o.anadirRuta(ruta);
			Document doc = parser.createDocument();

			// Documento resumen xml
			String pathResumenRuta = parser.writeDocumentToXml(doc);

			// Guarda resumen de la ruta en BaseX
			daoBaseX.insertarXml(pathResumenRuta, nombreRuta);

			// Abrir xml rutas.xml
			boolean existsRutas = daoBaseX.retrieveRutas();
			// Si existe un archivo rutas.xml en la base de datos.
			if (existsRutas) {
			    // Creamos un documento DOM con el archivo
			    // rutasrecuperado
			    Document docRutas = Util
				    .createNewDocumentToParse("/tmp/rutas_recuperado.xml");

			    Node newNode = doc.getFirstChild().cloneNode(true);

			    docRutas.adoptNode(newNode);
			    // Añade el nuevo resumen de ruta al final del
			    // documento con todas la rutas.
			    docRutas.getFirstChild().appendChild(newNode);

			    // Escribe el Document en un xml
			    Util.writeDocumentToFileXml(docRutas, "rutas");
			    //Util.printFileXml(docRutas);

			    daoBaseX.insertarXml("/tmp/rutas.xml", "rutas");

			} else {// Si no exite crearemos un nuevo xml
				// Crea un fichero provisional donde
				// escribiremos el nodo raiz(<rutas>)
				// File rutas = new File("rutas_nuevo.xml");

			    // Crea un Document DOM en blanco
			    Document docRutas = Util.createNewDocumentToParse();

			    Element root = docRutas.createElement("rutas");

			    // Añade el primer nodo raíz
			    docRutas.appendChild(root);

			    Node newNode = doc.getFirstChild().cloneNode(true);

			    docRutas.adoptNode(newNode);
			    // Añade el nuevo resumen de ruta al final del
			    // documento con todas la rutas.
			    docRutas.getFirstChild().appendChild(newNode);

			    // Escribe el Document en un xml
			    Util.writeDocumentToFileXml(docRutas, "rutas");
			    // Util.printFileXml(docRutas);

			    // Inserta el xml de todas las rutas con la nueva
			    // ruta añadida en BaseX
			    daoBaseX.insertarXml("/tmp/rutas.xml", "rutas");

			}

			/**
			 * AÑADIR LOS FICHEROS MULTIMEDIA EN BASEX
			 */
			if (args.length > 6) {
			    if (args[6].equals("-image") || args[6].equals("-video")) {

				// Guardar todos los argumentos en un nuevo
				// array.
				// Estos seran los nombre de los ficheros de la
				// ruta. Imagenes y videos.

				// Bucle que cuenta los ficheros media que se
				// desean
				// guardar.
				int nFicherosMedia = 0; // Contador de ficheros
							// media.
				for (int i = 6; i < args.length; i++) {
				    // Si el argumento empieza con '-' será una
				    // instrucción y no la contará en el total.
				    if (!args[i].matches("^-.+")) {
					nFicherosMedia++;
				    }
				}

				String[] ficherosMedia = new String[nFicherosMedia];
				int j = 0;
				for (int i = 6; i < args.length; i++) {
				    if (!args[i].matches("^-.+")) {
					// Guarda el nombre del fichero media en
					// el
					// array.
					ficherosMedia[j] = args[i];
					//System.out.println(ficherosMedia[j]);
					j++;
				    }
				}

				// Recorre los ficheros media que se quieren
				// insertar en BaseX.
				for (int i = 0; i < ficherosMedia.length; i++) {
				    // Consulta el id del último documento
				    // añadido a
				    // la bd postgres.
				    int idDocumento = daoSql.getIdDocumento();
				    // id del nuevo documento.
				    idDocumento = idDocumento + 1;

				    // Contruye nombre del fichero. Servirá para
				    // postgres y BaseX.
				    String nombreFichero = idRuta + args[1] + idDocumento;

				    // Insertar los fichero en BaseX.
				    daoBaseX.insertarFicheroMedia(nombreFichero);

				    // Insertar el nombre del fichero en la base
				    // de
				    // datos postgres.
				    daoSql.insertarDocumento(nombreFichero, idRuta);
				}

			    }
			}
			/**
			 * MOSTRAR RESUMEN DE TODAS LAS RUTAS DEL USUARIO EN
			 * HTML
			 */
		    } else if (args[4].equals("-output")) {

			String[] rutasUsuario = daoSql.getRutasUsuario(args[1]);
			if (rutasUsuario != null) {

			    Document doc = Util.createNewDocumentToParse();
			    Element root = doc.createElement("rutas");
			    // Añade el primer nodo raíz
			    doc.appendChild(root);

			    for (int i = 0; i < rutasUsuario.length; i++) {
				// Recupera una copia de los ficheros y los
				// guarda en /tmp/
				daoBaseX.retrieveResumenXml(rutasUsuario[i]);
				// System.out.println(rutasUsuario[i]);

				// Por cada fichero recuperamos en nodo
				// principal(sera la ruta)
				String ficheroRecuperado = "/tmp/" + rutasUsuario[i]
					+ "_recuperada.xml";
				// System.out.println(ficheroRecuperado);
				Document docFicheroRecuperado = Util
					.createNewDocumentToParse(ficheroRecuperado);

				Node newNode = docFicheroRecuperado.getFirstChild().cloneNode(
					true);

				doc.adoptNode(newNode);
				// Añade el nuevo resumen de ruta al final del
				// documento con todas la rutas del usuario.
				doc.getFirstChild().appendChild(newNode);

			    }
			    // Escribe el documento en un fichero xml
			    Util.writeDocumentToFileXml(doc, "rutas_usuario");

			    // Muestra el resumen de las rutas en el navegador
			    GenerateHtml.generateOutputHtml("rutas_usuario.xml", args[5],
				    "./src/model/plantilla_rutas_usuario.xsl");

			    // Añade la acción al historial
			    daoDb4o.addLogConsultarRutasUsuario(true, idUsuario);
			}
		    } else {
			// Añade la acción al historial
			if (args[4].equals("-ruta")) {
			    daoDb4o.addLogSubirRuta(false, idUsuario);
			} else {
			    daoDb4o.addLogConsultarRutasUsuario(false, idUsuario);
			}
			errorMessage = errorMessageGeneral;
			// Mostramos el mesaje de error por pantalla
			System.err.println(errorMessage);
		    }

		} else {
		    tag = "Error '" + args[0] + "': ";
		    // Discriminamos según el tipo de error.
		    if (!passwordCorrecto) {
			errorMessage = "El usuario o la contraseña son incorrectos";
		    } else {
			errorMessage = errorMessageGeneral;
		    }
		    // Mostramos el mesaje de error por pantalla
		    System.err.println(tag + errorMessage);
		}
		break;
	    /*
	     * VER UNA RUTA
	     * 
	     * En caso de aparecer la opción '-find' como primer argumento
	     * comprobaremos que el tipo y número de argumentos sea el correcto.
	     * La instrucción contendrá una id_ruta como segundo argumento y la
	     * opción '-output' como segundo. Ejemplo '-find 23 -output
	     * salida.html.
	     */
	    case "-find":
		if (args[2].equals("-output") && args.length == 4
			&& Util.validarFicheroSalida(args[3])) {

		    // Recupera el xml con la ruta
		    daoBaseX.retrieveResumenXml("ruta" + args[1]);

		    // Muestra el resumen de las rutas en el navegador
		    GenerateHtml.generateOutputHtml("ruta" + args[1] + "_recuperada.xml",
			    args[3], "./src/model/plantilla_ruta.xsl");
		    // Añade la acción al historial
		    daoDb4o.addLogVerRuta(true, -1);
		} else {
		    daoDb4o.addLogVerRuta(false, -1);
		    errorMessage = "Argumentos no válidos";
		}
		break;
	    /*
	     * CONSULTAR RUTAS
	     * 
	     * En caso de aparecer la opción '-output' como primer argumento
	     * comprobaremos que exista un segundo argumento donde indique el
	     * fichero de salida. Ejemplo '-output salida.html'.
	     */
	    case "-output":
		if (args.length == 2 && Util.validarFicheroSalida(args[1])) {
		    
		    // Recuperamos el xml con todas las rutas
		    daoBaseX.retrieveRutas();
		    
		    // Muestra el resumen de las rutas en el navegador
		    GenerateHtml.generateOutputHtml("rutas.xml", args[1],
			    "./src/model/plantilla_total_rutas.xsl");

		    // Añade la acción al historial
		    daoDb4o.addLogConsultarTotalRutas(true, -1);

		} else {
		    daoDb4o.addLogConsultarTotalRutas(false, -1);
		    errorMessage = "Argumentos no válidos";
		}
		break;
	    /**
	     * CONSULTAS DB4O
	     */
	    case "-db4o":
		String separator = "*****************************************************************";
		String mensaje = "Introduce el numero de la consulta a realizar o 'q' para salir";

		System.out.println(separator);
		System.out.println("CONSULTAS DB4O");
		System.out.println(separator);
		System.out.println("1 - Listar todos los usuarios");
		System.out.println("2 - Listar todas las rutas");
		System.out.println("3 - Listar todos las acciones");
		System.out.println("4 - Usuario que más rutas ha subido");
		System.out.println("5 - Usuario que ha subido más km de ruta");
		System.out.println("6 - Usuario que más acciones ha realizado");
		System.out.println("7 - Acciones realizadas por el usuario visitante");
		System.out.println("8 - Usuario que ha subido rutas con más desnivel");
		System.out.println("9 - De que usuario han sido las rutas más consultadas");
		System.out.println("10 - Ruta más corta");
		System.out.println("11 - Ruta más larga");
		System.out.println(mensaje);
		System.out.println(separator);

		Scanner sc = new Scanner(System.in);
		String opcion = "";

		do {
		    opcion = sc.nextLine();
		    switch (opcion) {
		    case "1":
			daoDb4o.listarUsuarios();
			break;
		    case "2":
			daoDb4o.listarRutas();
			break;
		    case "3":
			daoDb4o.listarHistorial();
			break;
		    case "4":
			daoDb4o.usuarioConMasRutas();
			break;
		    case "5":
			daoDb4o.usuarioConMasKm();
			break;
		    case "6":
			daoDb4o.usuarioConMasAcciones();
			break;
		    case "7":
			daoDb4o.getAccionesVisitante();
			break;
		    case "8":
			daoDb4o.usuarioConMasDesnivel();
			break;
		    case "9":
			daoDb4o.usuarioRutasMasConsultadas();
			break;
		    case "10":
			daoDb4o.rutaMasCorta();
			break;
		    case "11":
			daoDb4o.rutaMasLarga();
			break;
		    default:
			break;
		    }
		    if (!opcion.equals("q")) {
			System.out.println(separator + "\n" + mensaje + "\n" + separator);
		    }
		} while (!opcion.equals("q"));

		break;
	    default:
		errorMessage = "Instrucción no válida";
		System.out.println(errorMessage);
		break;
	    }
	} else {
	    System.out.println("Argumentos incorrectos");
	}
	daoDb4o.db.close();
    }
}
