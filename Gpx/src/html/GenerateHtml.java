package html;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class GenerateHtml {

    public static void generateOutputHtml(String sourceFile, String salida, String templateFile){
	try {
	    File sf = new File("/tmp/"+ sourceFile); // source file
	    File rf = new File(salida); // result file
	    File tf = new File(templateFile); // template file
	    TransformerFactory f = TransformerFactory.newInstance();
	    Transformer t = f.newTransformer(new StreamSource(tf));
	    Source s = new StreamSource(sf);
	    Result r = new StreamResult(rf);
	    t.transform(s, r);
	    //Ejecuta el html en el navegador
	    Desktop.getDesktop().open(rf);
	} catch (TransformerConfigurationException e) {
	    System.out.println(e.toString());
	} catch (TransformerException e) {
	    System.out.println(e.toString());
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
