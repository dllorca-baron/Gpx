/**
 * 
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Modelo clase Usuario.
 *
 */
public class Usuario {
    
    private int idUsuario;
    private String nombre;
    private List<Ruta> listaRutas;
    /** Total de rutas subidas */
    private int totalRutas;
    /** Número de acciones realizadas */
    private int acciones;
    /** Total kms subidos */
    private double kms;
    /** Media de desnivel de todas la rutas subidas */
    private double mediaDesnivel;
        
    public Usuario() {
	super();
	this.listaRutas = new ArrayList<Ruta>();
    }
    
    public Usuario(int idUsuario){
	super();
	this.idUsuario = idUsuario;
	this.listaRutas = new ArrayList<Ruta>();
    }
    
    public Usuario(int idUsuario, String nombre) {
	super();
	this.idUsuario = idUsuario;
	this.nombre = nombre;
	this.listaRutas = new ArrayList<Ruta>();
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void addRuta(Ruta ruta){
	this.listaRutas.add(ruta);
	addRutaToTotalRutas();
    }
    
    public void addRutaToTotalRutas(){
	this.totalRutas += 1;
    }
    
    public List<Ruta> getListaRutas() {
        return listaRutas;
    }

    public void setListaRutas(List<Ruta> listaRutas) {
        this.listaRutas = listaRutas;
    }
    
    public void addAcciones(){
	this.acciones = getAcciones() + 1;
    }
    public int getAcciones() {
        return this.acciones;
    }

    public void setAcciones(int acciones) {
        this.acciones = acciones;
    }
    
    public void addKms(double kms){
	this.kms = getKms() + kms;	
    }
    public double getKms() {
        return kms;
    }

    public void setKms(double kms) {
        this.kms = kms;
    }

    public double getMediaDesnivel() {
	List<Ruta> list = getListaRutas();
	double totalDesnivel = 0;
	for (Ruta ruta : list) {
	    totalDesnivel += ruta.getDesnivel();
	}
	setMediaDesnivel(totalDesnivel/list.size());
	return mediaDesnivel;
    }

    public void setMediaDesnivel(double mediaDesnivel) {
        this.mediaDesnivel = mediaDesnivel;
    }
    
    @Override
    public String toString() {
	return "Usuario [idUsuario=" + idUsuario + ", nombre=" + nombre + ", Rutas="
		+ totalRutas + ", Kms subidos=" + kms + "]";
    }
    
    
    
    
}
