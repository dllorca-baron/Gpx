/**
 * 
 */
package model;

/**
 * Modelo de la calse ruta.
 *
 */
public class Ruta {
    
    private int idRuta;
    private String nombre;
    private String descripcion;
    private double desnivel;
    private double distancia;
    private int idUsuario;
    
    
    public Ruta() {
	super();
    }


    public Ruta(int idRuta, String nombre, String descripcion, double desnivel,
	    double distancia, int idUsuario) {
	super();
	this.idRuta = idRuta;
	this.nombre = nombre;
	this.descripcion = descripcion;
	this.desnivel = desnivel;
	this.distancia = distancia;
	this.idUsuario = idUsuario;
    }


    public int getIdRuta() {
        return idRuta;
    }


    public void setIdRuta(int idRuta) {
        this.idRuta = idRuta;
    }


    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getDescripcion() {
        return descripcion;
    }


    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public double getDesnivel() {
        return desnivel;
    }


    public void setDesnivel(double desnivel) {
        this.desnivel = desnivel;
    }


    public double getDistancia() {
        return distancia;
    }


    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }


    public int getIdUsuario() {
        return idUsuario;
    }


    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }


    @Override
    public String toString() {
	return "Ruta [idRuta=" + idRuta + ", nombre=" + nombre +  ", desnivel=" + desnivel + ", distancia=" + distancia
		+ ", idUsuario=" + idUsuario + "]";
    }
    
    
    
}
