package model;
import java.util.ArrayList;
import java.util.List;


/**
 * Modelo clase Puntos
 * 
 * @author iam47873216
 *
 */
public class Puntos {
	
	public List<Punto> listaPuntos;
	public double distancia;

	public Puntos() {
		super();
		this.listaPuntos = new ArrayList<Punto>();
	}
	
	public void addPoint(Punto p){
		this.listaPuntos.add(p);
	}

	public List<Punto> getListaPuntos() {
		return listaPuntos;
	}

	public void setListaPuntos(List<Punto> listaPuntos) {
		this.listaPuntos = listaPuntos;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}
	
	

}
