package model;

public class ResumenRuta {
	
	// Atributos
	private String nombreRuta;
	private String descripcion;
	private String desnivelPositivo;
	// private String desnivelNegativo;
	private String distancia;
	
	// Constructores
	
	public ResumenRuta() {
	};
	
	public ResumenRuta(String nombreRuta, String descripcion,
			String desnivelPositivo, String distancia) {
		super();
		this.nombreRuta = nombreRuta;
		this.descripcion = descripcion;
		this.desnivelPositivo = desnivelPositivo;
		this.distancia = distancia;
	}
	
	// Getters y setters
	
	public String getNombreRuta() {
		return nombreRuta;
	}

	public void setNombreRuta(String nombreRuta) {
		this.nombreRuta = nombreRuta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDesnivelPositivo() {
		return desnivelPositivo;
	}

	public void setDesnivelPositivo(String desnivelPositivo) {
		this.desnivelPositivo = desnivelPositivo;
	}

	public String getDistancia() {
		return distancia;
	}

	public void setDistancia(String distancia) {
		this.distancia = distancia;
	}	
}
