<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" />
	<xsl:template match="ruta">
		<html>
			<head>
				<title>
					<xsl:text>GPX-Mostrar ruta</xsl:text>
				</title>
			</head>

			<body>
				<h2>
					<xsl:text>GPX-Mostrar ruta</xsl:text>
				</h2>
			</body>
			<ul>

				<li>
					<strong>
						<xsl:value-of select="nombre" />
					</strong>
					<br />
					<p>
						Descripción:
						<xsl:value-of select="descripcion" />
					</p>
					<p>
						Desnivel positivo:
						<xsl:value-of select="desnivelPositivo" />
					</p>
					<p>
						Distancia:
						<xsl:value-of select="distancia" />
					</p>
				</li>
			</ul>

		</html>
	</xsl:template>
</xsl:stylesheet>