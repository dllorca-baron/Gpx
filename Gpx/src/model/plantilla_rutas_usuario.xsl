<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" />
	<xsl:template match="rutas">
		<html>
			<head>
				<title>
					<xsl:text>GPX-Rutas Usuario</xsl:text>
				</title>
			</head>

			<body>
				<h2>
					<xsl:text>GPX-Rutas Usuario</xsl:text>
				</h2>
			</body>
			<ul>
				<xsl:for-each select="ruta">
					<li>
						<strong>
							<xsl:value-of select="nombre" />
						</strong>
						<br />
						<p>
							Descripción:
							<xsl:value-of select="descripcion" />
						</p>
						<p>
							Desnivel positivo:
							<xsl:value-of select="desnivelPositivo" />
						</p>
						<p>
							Distancia:
							<xsl:value-of select="distancia" />
						</p>
					</li>
					<hr />
				</xsl:for-each>
			</ul>

		</html>
	</xsl:template>
</xsl:stylesheet>