/**
 * 
 */
package model;

/**
 * Modelo del objecto Accion. Acciones realizadas por el usuario.
 * 
 * @author David
 *
 */
public class Accion {
    
    private String accion;
    private boolean resultado;
    private int idUsuario;
    
    
    public Accion() {
	super();
    }


    public Accion(String accion, boolean resultado, int idUsuario) {
	super();
	this.accion = accion;
	this.resultado = resultado;
	this.idUsuario = idUsuario;
    }


    public boolean isResultado() {
        return resultado;
    }


    public void setResultado(boolean resultado) {
        this.resultado = resultado;
    }


    public int getUsuario() {
        return idUsuario;
    }


    public void setUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
	String mensajeResultado = "satisfactorio";
	if(!resultado){
	    mensajeResultado = "error";
	}
	return "Accion [accion= " + accion + ", resultado= " + mensajeResultado + ", usuario= "
		+ idUsuario + "]";
    }    
    
}
