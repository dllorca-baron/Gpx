package util;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Util {
    
    /** List con las opciones válidas */
    private static List<String> opcionesList = Arrays.asList("-add", "-user", "-pass",
	    "-ruta", "-image", "-video", "-output", "-find", "-db4o");
    
    /**
     * Valida que todos los argumentos introducidos sean correctos.
     * 
     * @param args
     * @return true si los argumentos son correctos o falso en caso contrario.
     */
    public static boolean validarArgumentos(String[] args) {

	// La instrucción mas corta la forma únicamente 2 argumentos.
	if (args.length > 1) {

	    // Recorre todos los argumentos de la instrucción
	    for (int i = 0; i < args.length; i++) {
		// En caso de encontrar un argumento que empiece por '-'
		// comprueba que este sea un comando válido.
		if (args[i].matches("^-.+") && !opcionesList.contains(args[i])) {
		    // Encuentra un '-comando' inexistente en la lista.
		    return false;
		}
	    }
	}
	// Si los argumentos son correctos.
	return true;

    }

    /**
     * Valida fichero de salida correcto.
     * 
     * @param fichero
     * @return true si el fichero tiene extensión .html y false en caso
     *         contrario.
     */
    public static boolean validarFicheroSalida(String fichero) {
	if (fichero.matches(".*\\.html$")) {
	    return true;
	}
	return false;
    }
	/**
	 * DOM
	 */

	/**
	 * Metodos DOM
	 * 
	 * node.getNodeName() -> devuelve String con el nombre del tag del Nodo.
	 * node.getTextContent() -> devuelve String con el contenido del Nodo.
	 * 
	 * node.getAttributes(); -> devuelve NamedNodeMap con los atributos.
	 * Node.hasChildNodes()
	 * 
	 * element.setAttributeNode(attr); -> añade attribute al elemento.
	 */

	/**
	 * Crea un Objeto Document para escribir xml.
	 * 
	 * @return Document donde escribiremos el esquema xml.
	 */
	public static Document createNewDocumentToParse() {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// A�adimos elemento raiz
		Document document = docBuilder.newDocument();

		return document;
	}

	/**
	 * Crea un Objeto Document para parsear un archivo xml existente.
	 * 
	 * @param filename
	 *            nombre del archivo xml que desea parsear.
	 * @return Document donde leeremos el esquema xml.
	 */
	public static Document createNewDocumentToParse(String filename) {

		File xml = new File(filename);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		Document document = null;
		try {
			document = dbFactory.newDocumentBuilder().parse(xml);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		document.getDocumentElement().normalize(); // No tiene en cuenta los
													// saltos de línea
		return document;

	}

	/**
	 * Devuelve el nombre del nodo principal del document.
	 * 
	 * @param document
	 * @return String con el nombre del nodo principal del document.
	 */
	public static Element getRootNode(Document document) {

		return document.getDocumentElement();

	}

	/**
	 * Muestra por consola el nodo principal del document.
	 * 
	 * @param document
	 */
	public static void printRootNodeName(Document document) {

		// Nodo principal
		System.out.println("Nodo principal: <"
				+ document.getDocumentElement().getNodeName() + ">");

	}

	/**
	 * Muestra por consola lo valores del nodo. Nombre, Tipo, Valor y Contenido.
	 * 
	 * @param node
	 */
	public void printInfoNode(Node node) {

		if (node.getNodeType() == Node.ELEMENT_NODE) {
			System.out.println("Elemento actual Nombre: " + node.getNodeName());
			System.out.println("Elemento actual Tipo: " + node.getNodeType());
			System.out.println("Elemento actual Valor: " + node.getNodeValue());
			System.out.println("Elemento actual Contenido"
					+ node.getTextContent());
		} else {
			System.err.println("node.getNodeType() != Node.ELEMENT_NODE");
		}

	}

	/**
	 * Muestra por consola los valores de los nodos de una lista de nodos.
	 * Nombre, Tipo, Valor y Contenido.
	 * 
	 * @param nodeList
	 */
	public void printInfoNode(NodeList nodeList) {

		for (int i = 0; i < nodeList.getLength(); i++) {
			printInfoNode(nodeList.item(i));
		}

	}

	/**
	 * Muestra por pantalla los atributos de un nodo.
	 * 
	 * @param node
	 */
	public static void printInfoAttributesNode(Node node) {

		NamedNodeMap nm = node.getAttributes();
		for (int i = 0; i < nm.getLength(); i++) {
			System.out.println("Atributo de <" + node.getNodeName() + ">: "
					+ nm.item(i).getNodeName() + " = "
					+ nm.item(i).getNodeValue());
		}

	}

	/**
	 * Escribe el contenido de un Document en un archivo .xml.
	 * 
	 * @param document
	 * @param filename
	 *            nombre del archivo (sin la extensión .xml).
	 */
	public static String writeDocumentToFileXml(Document document, String filename) {

		// escribimos el contenido en un fichero
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// XML indentado
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(document);
		String path = "/tmp/" + filename + ".xml";
		StreamResult result = new StreamResult(new File(path));

		// Mostramos por consola
		// StreamResult result = new StreamResult(System.out);

		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Mensaje informando si el fichero se ha escrito correctamenta.
//		System.out.println("Todo OK, fichero guardado!");
		return path;

	}

	/**
	 * Añade un Element al node raíz de un Document xml.
	 * 
	 * @param document
	 * @param newTagNameElement
	 *            nombre del nuevo elemento.
	 */
	public static void addElementToRoot(Document document,
			String newTagNameElement) {

		// Obtenemos el nodo principal.
		Element root = Util.getRootNode(document);
		// Creamos el nuevo elemento.
		Element element = document.createElement(newTagNameElement); // Nombre
																		// del
																		// tag
		// Añade elemeneto a nodo principal.
		root.appendChild(element); // Añadimos el nodo por el final.

	}

	/**
	 * Añadimos un nuevo elemento a un nodo.
	 * 
	 * @param document
	 * @param parentElement elemento padre
	 * @param newTagNameElement nombre del nuevo elemento.
	 */
	public static void addChildElement(Document document,
			Element parentElement, String newTagNameElement) {

		// Creamos el nuevo elemento.
		Element element = document.createElement(newTagNameElement); // Nombre
																		// del
																		// tag
		// Añade elemeneto a nodo principal.
		parentElement.appendChild(element); // Añadimos el nodo por el final.

	}
	
	/**
	 * Crea un atributo de un elemento xml.
	 * 
	 * @param document
	 * @param attributeTag tag del atributo.
	 * @param attributeValue valor del atributo.
	 * @returna atributo xml.
	 */
	public static Attr createAttribute(Document document, String attributeTag,
			String attributeValue) {

		Attr attr = document.createAttribute(attributeTag);
		attr.setValue(attributeValue);
		return attr;		
		//element.setAttributeNode(attr); //añadir attr a un Element
	}
	
	public static void printFileXml(Document document) {

		try {
			// escribimos el contenido en un fichero
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			// XML indentado
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(document);

			// Mostramos por consola
			StreamResult result = new StreamResult(System.out);
			transformer.transform(source, result);

		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
}
