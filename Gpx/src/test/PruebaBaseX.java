package test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.Check;

import server.RunServerBaseX;
import controller.DaoBaseX;

public class PruebaBaseX {

    public static void main(String[] args) throws IOException {

//	 // arranca el servidor
//	 try {
//	 new Thread(new RunServerBaseX()).join();
//	 } catch (InterruptedException e) {
//	 // TODO Auto-generated catch block
//	 e.printStackTrace();
//	 }

	 // create session
	 // cliente para conectarnos al servidor
	 ClientSession session = new ClientSession("localhost", 1984, "admin",
	 "admin");

//	// iniciamos els servidor
//	BaseXServer server = new BaseXServer();
//	// cliente para conectarnos al servidor
//	ClientSession session = new ClientSession("localhost", 1984, "admin", "admin");

	try {
	    
//	    DaoBaseX dao = new DaoBaseX();
	    
	    

	    
	    
//	    // create empty database
	     session.execute("create db databaseGpx");
	     System.out.println(session.info());
//	     
//	     DaoBaseX dao = new DaoBaseX();
//	     
//	     dao.insertarFicheroMedia("ball.png");
//	     System.out.println(session.info());
//	     
//	    //
//	    // session.execute(new CreateDB("input"));
//	    // System.out.println(session.info());
//
	    // Crea la base de datos si no existe y la abre en caso de existir.
	    session.execute("check databaseGpx");
	    System.out.println(session.info());
//	    
//	    
//	    String query = "xquery file:exists('ruta48.xml' as item()) as xs:boolean";
//	    String b = session.execute(query);
//	    
////	    session.execute("xquery file:exists(rutas.xml as item()) as xs:boolean");
//	    System.out.println(session.info() + " result: " + b);
//	    
	    	    
	    // // session.execute(new DropDB("input"));
	    // // System.out.println(session.info());
	    //
	    // session.execute(new DropDB("database"));
	    // System.out.println(session.info());

//	    File file = new File("./ball.png");
//
//	    // define input stream
//	    final byte[] bytes = new byte[(int) file.length()];
//	    for (int b = 0; b < bytes.length; b++)
//		bytes[b] = (byte) b;
//	    final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);

//	     // Añadir un xml
//	     Path path = Paths.get("./ruta.gpx");
//	     InputStream is = new
//	     ByteArrayInputStream(Files.readAllBytes(path));
//	     session.add("ruta.xml", is);
//	     System.out.println(session.info());

	    // add document
//	    session.store("img/ball.png", bais);
//	    System.out.println(session.info());

	    // ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    //
	    // session.setOutputStream(baos);
	    // session.execute("retrieve ball.png");
	    // System.out.println(session.info());
	    //
	    // FileOutputStream fileout = new FileOutputStream(new File(
	    // "recuperado"));
	    // baos.writeTo(fileout);
	    // receive data
//	    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
//	    session.execute("retrieve ball.png");
//
//	    FileOutputStream fileout = new FileOutputStream(new File("recuperado"));
//	    baos.writeTo(fileout);
//
//	    // should always yield true
//	    if (Arrays.equals(bytes, baos.toByteArray())) {
//		System.out.println("Stored and retrieved bytes are equal.");
//	    } else {
//		System.err.println("Stored and retrieved bytes differ!");
//	    }
//
//	     // drop database
	     session.execute("drop db databaseGpx");
	     System.out.println(session.info());

	} finally {
	    // close session
	    session.close();
	    System.out.println("session closed");
	}
    }

}
