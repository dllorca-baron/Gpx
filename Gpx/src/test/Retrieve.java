package test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.Check;

public class Retrieve {

    public static void main(String[] args) {

	try {
	    // iniciamos els servidor
	    BaseXServer server = new BaseXServer();
	    // cliente para conectarnos al servidor
	    ClientSession session = new ClientSession("localhost", 1984, "admin", "admin");

	    // Crea la base de datos si no existe y la abre en caso de existir.
	    session.execute(new Check("databaseGpx"));
	    System.out.println(session.info());

	    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    session.setOutputStream(baos);
	    session.execute("retrieve ruta.xml");
	    System.out.println(session.info());
	    
	    FileOutputStream fileout = new FileOutputStream(new File("recuperado"));
	    baos.writeTo(fileout);
	    
	    
	    
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

}
