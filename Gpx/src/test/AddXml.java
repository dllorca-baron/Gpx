package test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.basex.BaseXServer;
import org.basex.api.client.ClientSession;
import org.basex.core.cmd.Check;

public class AddXml {

    public static void main(String[] args) {
	try {
	    // iniciamos els servidor
	    BaseXServer server = new BaseXServer();
	    // cliente para conectarnos al servidor
	    ClientSession session = new ClientSession("localhost", 1984, "admin", "admin");

	    // Crear una base de datos y añadir los xml dados.
	    session.execute(new Check("databaseGpx"));

	    // Path path = Paths.get("./ruta.gpx");
	    // InputStream is = new
	    // ByteArrayInputStream(Files.readAllBytes(path));
	    // session.store("ruta.xml", is);

	    // Añadir un xml
	    Path path = Paths.get("./ruta.gpx");
	    InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));
	    session.add("rutas.xml", is);
	    System.out.println(session.info());

	    ByteArrayOutputStream baos = new ByteArrayOutputStream();

	    session.setOutputStream(baos);
	    session.execute("retrieve ruta5.xml");

	    FileOutputStream fileout = new FileOutputStream(new File("recuperado5"));
	    baos.writeTo(fileout);

	    server.stop();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

}
